#include <stdio.h>

#define NROWS 8192
#define NCOLS 8192
#define NTIMES 10

char matrix[NROWS][NCOLS];

int main(void)
{
	int i, j, rep;

	for (rep = 0; rep < NTIMES; rep++)
	{
		for (i = 0; i < NROWS; i++)
		{
			for(j = 0; j < NCOLS; j++)
			{
				matrix[i][j] = 'A';
				if (rep == 0 && i < 4)
				{
					if (j == 0)
					{
						printf("Begin row\n");
					}
					
					printf("%p\n", &(matrix[i][j]));
				}
			}
		}
	}
}
