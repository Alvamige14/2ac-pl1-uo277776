#include <stdio.h>
#include <string.h>

struct _Person
{
	char name[30];
	int heightcm;
	double weightkg;
};

typedef struct _Person Person;

int main(int argc, char* argv[])
{
	Person Peter;
	strcpy(Peter.name, "Peter");
	Peter.heightcm = 175;
	Peter.weightkg = 70;

	printf("%s's height: %d cm; %s's weight: %f kg\n", Peter.name, Peter.heightcm, Peter.name, Peter.weightkg);
	
	return 0;
}
