.data
	string: .asciiz "Helloooo"
	result: .word 0
.text
	main:
		xor r1, r1, r1
		xor r2, r2, r2
	loop: 
		lb r4, string(r2)
		beqz r4, end
		daddui r1, r1, 1
		daddui r2, r2, 1
		j loop
	end:
		sd r1, result(r0)
		halt